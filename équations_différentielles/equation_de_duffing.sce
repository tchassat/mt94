// équation de duffing

function dXdt = duffing(t, X)
    k = 0.1;
    dXdt = [X(2)
            -k*X(2)+X(1)-X(1)^3];
end 

clf
subplot(2,2,1)
X1=linspace(-0.1,0.1,10);
X2=linspace(-0.1,0.1,10);
fchamp(duffing,0,X1,X2)
plot(0,0,"xr")
t=linspace(0,5,200);
for X10=X1
    for X20=X2
        X=ode([X10;X20],0,t, duffing);
        plot(X(1,:),X(2,:))
    end 
end 
gca().data_bounds = [X1(1) X1($) X2(1) X2($)]
title("$X^* = (0,0)^\top$")

subplot(2,2,2)
X1=linspace(-0.1,0.1,10)+1;
X2=linspace(-0.1,0.1,10);
fchamp(duffing,0,X1,X2)
plot(1,0,"xr")
t=linspace(0,5,200);
for X10=X1
    for X20=X2
        X=ode([X10;X20],0,t, duffing);
        plot(X(1,:),X(2,:))
    end 
end 
gca().data_bounds = [X1(1) X1($) X2(1) X2($)]
title("$X^* = (1,0)^\top$")


subplot(2,2,3)
X1=linspace(-0.1,0.1,10)-1;
X2=linspace(-0.1,0.1,10);
fchamp(duffing,0,X1,X2)
plot(-1,0,"xr")
t=linspace(0,5,200);
for X10=X1
    for X20=X2
        X=ode([X10;X20],0,t, duffing);
        plot(X(1,:),X(2,:))
    end 
end 
gca().data_bounds = [X1(1) X1($) X2(1) X2($)]
title("$X^* = (-1,0)^\top$")


subplot(2,2,4)
X1=linspace(-2,2,10);
X2=linspace(-2,2,10);
fchamp(duffing,0,X1,X2)
plot(-1,0,"xr",1,0,"xr", 0,0,"xr" )
t=linspace(0,5,200);
for X10=X1
    for X20=X2
        X=ode([X10;X20],0,t, duffing);
        plot(X(1,:),X(2,:))
    end 
end 
gca().data_bounds = [X1(1) X1($) X2(1) X2($)]

