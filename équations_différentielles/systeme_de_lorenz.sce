// système de Lorenz

function dxdt = lorenz(t,x)
    X1=x(1);
    X2=x(2);
    X3=x(3);
    dX1=Sigma.*(X2-X1); 
    dX2=Rho*X1-X2-X1.*X3;
    dX3=X1.*X2-Beta.*X3;
    dxdt =[dX1, dX2, dX3]; 
endfunction

Sigma=10;
Rho=28; 
Beta= (8)./3;

t=linspace(0,25*%pi,7000)
X0=[0.0000000000001;0.0000000000001;0.0000000000000]

X=ode(X0,0,t,lorenz); 

clf


param3d(X(1,:),X(2,:), X(3,:));
e=gce(); 
e.foreground=color(206, 51, 221);
a=gca(); 
a.rotation_angles=[100 105];


