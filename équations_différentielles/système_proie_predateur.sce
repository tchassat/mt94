// système proie-predateur 

scf(0) 
clf

scf(1)
clf

function dxdt = proie_predateur(t,x)
    X1=x(1);
    X2=x(2);
    dX1=X1.*(alpha - betta.*X2); 
    dX2=-X2.*(gama - delta.*X1)
    dxdt =[dX1, dX2]; 
endfunction

alpha=2/3;
betta=4/3; 
gama=1; 
delta=1;

t=0:0.01:20
clf
for i=0.9:0.1:1.8
    X0=[i,1]';
    X=ode(X0,0,t,proie_predateur);
    
    scf(1)
    plot(X(1,:),X(2,:))
    title("predateurs en fonction de proies")
    plot(1,0.5,"rx")
    
end




for i=0.9:0.1:1.8;
    X0=[1,i]';
    X=ode(X0,0,t,proie_predateur);
    
    scf(0)
    subplot(2,1,1)
    plot(t,X(1,:))
    title("proies en fonction du temps")
    
    scf(0)
    subplot(2,1,2)
    plot(t,X(2,:))
    title("prédateurs en fonction du temps")
end
