// comparaison des différents schémas de résolution d'équations différentielles

// l'exectution de ce programme nécessite la presence du programme 'td_equadiff_schemas.sce' dans le repertoire)

function y=sol(t)
    y = 1-exp(-t.^2/2);
endfunction

function dydt=f1(t,y)
    dydt = -t*y+t;
endfunction


exec("td_equadiff_schemas.sce",-1);

T=4;
//y=euler(y0,t,f1)

N = [1,3,5,10,22, 47, 100, 216, 465, 1000]


err=zeros(5,length(N));
for i =1:length(N)
    h=T/N(i);
    t=0:h:T;
    err(1,i) = max(abs(sol(t)-euler(0,t,f1)));
    err(2,i) = max(abs(sol(t)-euler_cauchy(0,t,f1)));
    err(3,i) = max(abs(sol(t)-point_milieu(0,t,f1)));
    err(4,i) = max(abs(sol(t)-RK4(0,t,f1)));
    err(5,i) = max(abs(sol(t)-ode(0,0,t,f1)));
end

clf
h = T./N;    
plot(h,err,"-o");
gca().log_flags = "ll";
legend('Euler','Euler-Cauchy','point milieu','RK4','ode',-1)
xlabel('h')
ylabel('erreur par rapport à la solution théorique')
title('Graphique de l erreur des différents schémas en fonction de h, en échelle logarithmique')

isoview off
