// le pendule

//comparaison de la solution linéarisé et celle donnée par ode

function dydt=f(t,y)
    theta=y(1);
    thetaprime=y(2);
    g = 9.81;
    L = 1;
    dydt = [thetaprime 
            -g/L*sin(theta)]
endfunction

g = 9.81;
L = 1;

t = linspace(0,10,1000);

clf 

y01 = [%pi/32;0];
y1 = ode(y01,0,t,f)
theta1 = y1(1,:)
subplot(2,2,1)
plot(t,theta1,t,y01(1)*cos(sqrt(g/L)*t))

title("$ y_0=\pi/32$")


y02 = [%pi/8;0];
y2 = ode(y02,0,t,f)
theta2 = y2(1,:)
subplot(2,2,2)
plot(t,theta2,t,y02(1)*cos(sqrt(g/L)*t))

title("$ y_0=\pi/8$")

y03 = [3*%pi/4;0];
y3 = ode(y03,0,t,f)
theta3 = y3(1,:)
subplot(2,2,3)
plot(t,theta3,t,y03(1)*cos(sqrt(g/L)*t))

title("$y_0=3\pi/4$")

y04 = [63*%pi/64;0];
y4 = ode(y04,0,t,f)
theta4 = y4(1,:)
subplot(2,2,4)
plot(t,theta4,t,y04(1)*cos(sqrt(g/L)*t))

title("$y_0=63\pi/64$")

