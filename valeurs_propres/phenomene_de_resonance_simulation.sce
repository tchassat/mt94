// simulation du système masse-ressort à 2ddl.

// l'execution de ce programme nécessite l'execution préalable du programme 'anime_os.sci' fournit par Stéphane Mottelet. 


function vprime = F(t,v)
    u=v(1:2)
    uprime=v(3:4)
    vprime=[uprime
            -A*u+b*sin(omega*t)]
endfunction

k1 = 5
k2 = 10
m1 = 0.1
m2 = 0.1

A=[(k1+k2)/m1 -k2/m1
    -k2/m2 k2/m2]
b = [0; 1/m2]

// Calcul des valeurs propres 
x = rand(2,1)
TOL = 1e-6
for k=1:100
    y=A*x // Puissance
    //y=A\x   // Puissance inverse
    if norm(y - (x'*y)*x) < TOL
        break
    end
    x=y/norm(y)
end
lambda = x'*(A*x)
disp(x, lambda)

omega = sqrt(lambda)

t= linspace(0,10,1000)
v=ode(zeros(4,1),0,t,F)
clf
subplot(2,1,1)
plot(t,v(1:2,:))
title('Deuxième valeur propre')

subplot(2,1,2)
anime_os(t,v,m1,m2,1,1)


