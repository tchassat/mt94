// détermination des valeurs propres d'une matrice - algorithme de la puissance - algorithme de la puissanceinverse - de la macro spec

// ici, les valeurs propres de A, problème de résonance d'un système mécanique à deux degré de libertés.

k1 = 5
k2 = 10
m1 = 0.1
m2 = 0.1

A=[(k1+k2)/m1 -k2/m1
    -k2/m2 k2/m2]

x = rand(2,1)
TOL = 1e-6
for k=1:100
    y=A*x // Puissance
    //y=A\x   // Puissance inverse
    if norm(y - (x'*y)*x) < TOL
        break
    end
    x=y/norm(y)
end

lambda = x'*(A*x)
disp(x, lambda)

disp(spec(A))
