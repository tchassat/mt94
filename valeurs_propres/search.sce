// Complements Algorithme PageRank pour MT94
// S. Mottelet, UTC
// Jeu 16 mai 2020 13:17:00 CEST

function search(U,x,term) // fonction de recherche par mot-clés contenus dans les URL
// U : matrice des URL
// x : PageRank
// term : mot-cle recherche

ind=[]
for i=1:size(U,1)
  if ~isempty(strindex(U(i),term))
    ind=[ind i] 
  end 
end 

if ~isempty(ind)
  xf=x(ind)
  Uf=U(ind)
  [xf,k]=gsort(xf)
  Uf=Uf(k)
  printf("Search result for ""%s"":\n\n",term)
  printf("%8s | %-40s\n","Rank","URL")
  printf("---------|---------------------------\n")  
  for i=1:min(10,length(ind))
      printf("%8.6f | %s\n",xf(i),Uf(i))
  end
  printf("\n")
end

endfunction
