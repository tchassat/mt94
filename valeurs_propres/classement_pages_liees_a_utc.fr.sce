// alogorithme de Pagerank

//l'execution de ce programme necessite la presence de 'www_utc_fr.sod" dans le répertoire, ainsi que l'exectution du programme 'search.sce'.

load www_utc_fr.sod
ij=spget(C)

                    //tracer la matrice C
scf(0)
clf
plot(ij(:,2),ij(:,1), '.', "markersize",2)
gca().axes_reverse(2)="on"
isoview

                    //attribuer une note aux pages 
[n,n]=size(C)
d = sum(C,2)
d(d>0) = 1./d(d>0)
e = ones(n,1)
f = ones(n,1)
f(d==0) = 0

pi = zeros(1,n)
pi(1)=1

q = 0.85
dt=d'
et=e'

MAX=1000
for k=1:MAX
    newpi = q*(pi.*dt)*C + (pi*(e-q*f))*et/n
    if norm(pi-newpi)< 1e-6
        break
    end
    pi=newpi
end

                    //tracer la valeur de pi pour chaque page
scf(1)
clf
plot(pi)


                    //page avec le rang le plus élevé : 
[pmax,i]=max(pi)
disp(U(i))


                   //afficher les 10 pages les mieux notées
scf(2)
clf
[pis, inds]=gsort(pi,"g","d")
plot(gsort(pi,"g","d"),"r")
disp(U(inds(1:10)))

                  //utilisation de la fonction search
search(U,pi,'mecanique')
