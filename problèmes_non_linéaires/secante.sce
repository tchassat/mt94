// Méthode de la sécante

function y=f(x) //definition de la fonction
    y = x^2 -2; 
end 

a=1;    //definition des bornes 
b=2;

ITMAX=100;  //nombre maximal d iteration
TOL=1e-10;  //tolerance

clear x
x=[a;b];   //valeur initial de x
for k=2:ITMAX
    df = (f(x(k))-f(x(k-1)))/(x(k)-x(k-1))
    x(k+1)= x(k) - f(x(k))/df;           //???
    if abs(f(x(k))) < TOL   //si |f(x)| est inf. a la tolerence, stop
        break;
    end 
end

e = abs(x-sqrt(2))     //calcul de l erreur. x-rac2
e(e==0)=[];


clf
plot(e(1:$-1),e(2:$),"-o")  //vitesse de convergence : erreur à l'iteréation k+1 en fonction de l'erreur à l itération k
gca().log_flags="ll"
isoview
xlabel("$\text{erreur à l itération } k$")
ylabel("$\text{erreur à l itération } k+1 $")
title("$\text{Graphique de l erreur à l itération } k+1 \text{ en fonction de l erreur à l itération } k \text{ en échelle logarithmique}$")



disp(f(x),x)

disp(k)     //affichage du nombre d iteration


[p,q] = reglin(log(e(1:$-1))',log(e(2:$))') //p est le "p" l'ordre de convvergence


disp(p)
