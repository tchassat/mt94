// Comparaisons des différentes méthodes 

// NB : ce programme nécessite les programmes pointfixe.sce, newton.sce, secante.sce et dichot.sce

exec("dichot.sce",-1);
e1 = e; 
x1=x;
exec("pointfixe.sce",-1);
e2 = e; 
x2=x;
exec("newton.sce",-1);
e3 = e; 
x3=x;
exec("secante.sce",-1);
e4 = e; 
x4=x;


scf(0);clf; plot(x1, "-o")
plot(x2, "-og")
plot(x3, "-or")
//plot(x4, "-om")
xlabel("nombre d itération")
ylabel("valeur de x")
title("Graphique du nombre d itération necessaire, par méthodes, pour converger vers le résultat")
legend("dichotomie","point fixe","newton ", pos=-1)


scf(1)
clf
plot(e1(1:$-1),e1(2:$),"-o")  
plot(e2(1:$-1),e2(2:$),"-og")  
plot(e3(1:$-1),e3(2:$),"-or")  
plot(e4(1:$-1),e4(2:$),"-om")  


gca().log_flags="ll"
isoview
xlabel("erreur à l itération k")
ylabel("erreur à l itération k+1")
title("Graphique de l erreur à l itération k+1 en fonction de l erreur à l itération k en échelle logarithmique")
legend("dichotomie","point fixe","newton ","sécante", pos=-1)
