// Méthode de dichotomie

function y=f(x) //definition de la fonction
    y = x^2 -2; 
end 

a=1;    //definition des bornes 
b=2;

ITMAX=100;  //nombre maximal d iteration
TOL=1e-10;  //tolerance

clear x     
for k=1:ITMAX
    x(k)=(a+b)/2;           //x prend la valeur de la moitie de l'intervalle
    if abs(f(x(k))) < TOL   //si |f(x)| est inf. a la tolerence, stop
        break;
    end 
    if f(a)*f(x(k)) <= 0    //sinon, si f(a) et f(x) sont de signe contraire, alors on reduit l intervalle et b devient x
        b=x(k); 
    else
        a=x(k);             //sinon, a devient x (on reduit par la gauche)
    end
end 

e = abs(x-sqrt(2))     //calcul de l erreur. x-rac2
clf 
//plot(e, "-o")
plot(e(1:$-1),e(2:$),"-o")  //vitesse de convergence : erreur à l'iteréation k+1 'n fonction de l'erreur à l itération k
gca().log_flags="ll"
isoview
xlabel("$\text{erreur à l itération } k$")
ylabel("$\text{erreur à l itération } k+1 $")
title("$\text{Graphique de l erreur à l itération } k+1 \text{ en fonction de l erreur à l itération } k \text{ en échelle logarithmique}$")


disp(f(x),x)

disp(k)     //affichage du nombre d iteration


[p,q] = reglin(log(e(1:$-1))',log(e(2:$))') //p est le "p" l'ordre de convvergence


disp(p)
