// fractale de newton d'ordre 3

n = 2000;
x = linspace(-1,1,n);
[X,Y] = meshgrid(x,x);
Z = complex(X,Y);

for k=1:20
    Z = 2/3*Z+(1/3)./(Z.*Z);
end

rac = roots(%z^3-1);
A = ones(n,n);
for i = 1:length(rac)
    A(abs(Z-rac(i))<1e-10) = i+1;
end

clf
gcf().color_map = [1 1 1;1 0 0;0 0.7 0;0 0 1]
plot(-1,-1,1,1)
Matplot1(A,[-1,-1,1,1])
gca().tight_limits = %t;
isoview
