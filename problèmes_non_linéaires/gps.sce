// GPS

function gps = f(X)         //defintion de la fonction
    gps = [norm(X-S1)^2-d(1)^2
           norm(X-S2)^2-d(2)^2
           norm(X-S3)^2-d(3)^2] //la fonction est un vecteur colonne; chaque colone est égale à la norme au carré de X-S1 - la première composante du vecteur d au carré
endfunction


function out = jac_c(X)
    out = imag([
    f(X+%i*h*[1;0;0]) f(X+%i*h*[0;1;0]) f(X+%i*h*[0;0;1])
    ])/h
endfunction



function out = deriv(X)       //calcul de la jacobienne
    out = 2*[(X-S1)'
             (X-S2)'
             (X-S3)']
endfunction

    S1 = [-11716.227778,-10118.754628,21741.083973]';   //definition des constantes du probleme
    S2 = [-12082.643974,-20428.242179,11741.374154]';
    S3 = [14373.286650,-10448.439349,19596.404858]';
    d = [22163.847742,21492.777482,21492.469326];

X0=[0 0 0]'; 

[X, v, info]=fsolve(X0, f, deriv);

disp(X, v, info) 

disp(norm(X)) //pour comparer avec le rayon de la terre 
