// Cinématique inverse : tracer d'un cube

// l'execution de ce programme nécessite l'execution du programme visuArm.sce

//on cherche theta tel que M(theta)-A=0

function out=f(theta, A)
    M= [l*cos(theta(1:2))*cos(theta(3))
        l*cos(theta(1:2))*sin(theta(3))
        l*sin(theta(1:2))]; 
    out = M-A; 
end 

l = [1, 2/3];
h=0

A=[h;h;0.5];
B=[0.5+h;h;0.5];
C=[0.5+h;0.5+h;0.5];
D=[h;0.5+h;0.5];
E=[h;h;0.5+0.5];
F=[0.5+h;h;0.5+0.5];
G=[0.5+h;0.5+h;0.5+0.5];
H=[h;0.5+h;0.5+0.5];



coords = [linspace(A,B,100)..
linspace(B,C,100)..
linspace(C,D,100)..
linspace(D,A,100)..
linspace(A,E,100)..
linspace(E,F,100)..
linspace(F,G,100)..
linspace(G,H,100)..
linspace(H,E,100)..
linspace(E,F,100)..
linspace(F,B,100)..
linspace(B,C,100)..
linspace(C,G,100)..
linspace(G,H,100)..
linspace(H,D,100)..
]

theta = [0.1;0.1;0.1];

clf
for X = coords
    [theta,v,info]=fsolve(theta,list(f,X));
    disp(info)
    visuArm(theta,l,%t)
    gca().rotation_angles=[78   36];
end



