//fractale de newton d'ordre 5

n = 2000;
x = linspace(-1,1,n);
[X,Y] = meshgrid(x,x);
Z = complex(X,Y);

for k=1:70
    Z = Z - (Z.^5-1)./(5.*Z.^4);
end

rac = roots(%z^5-1);
A = ones(n,n);
for i = 1:length(rac)
    A(abs(Z-rac(i))<1e-10) = i+1;
end

clf
gcf().color_map = [1 1 1; 1 0.75 0;1 0 0;0 0 1;0 0.69 0.94;0.81 0.2 0.87]
plot(-1,-1,1,1)
Matplot1(A,[-1,-1,1,1])
gca().tight_limits = %t;
isoview
