//division euclidienne

function [quotient,reste]=divEucl(a,b) //definition de la fonction : elle prend une matrice 1x2 en entree et renvoie une matrice 1x2.
    
    reste=a;        //au début, le reste est le dividende 
    quotient=0;     //le quotient est nul
    while reste>=b  //tan que le rest est superieur au diviseur, on exectue les operations suivantes : 
            reste=reste-b;      //le reste prend la valeur reste-diviseur
            quotient=quotient+1 //le quotient prend la valeur quotient+1
     end
endfunction
