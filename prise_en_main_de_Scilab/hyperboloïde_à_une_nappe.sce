//hyperboloïde à une nappe

teta=linspace(0,2*%pi, 20);
p=linspace(1,7,20);

[TETA,P]=meshgrid(teta,p)

a=2;
b=3;
c=4;

X=a.*P.*cos(TETA);
Y=b.*P.*sin(TETA);
Z=-c.*sqrt(P.^2 -1);

T=a.*P.*cos(TETA);
N=b.*P.*sin(TETA);
M=+c.*sqrt(P.^2 -1);


figure(0)
clf
mesh(X,Y,Z)
set(gcf(),"color_map",jetcolormap(128));
surf(X,Y,Z)
set(gce(),"color_flag",3);

mesh(T,N,M)
set(gcf(),"color_map",jetcolormap(128));
surf(T,N,M)
set(gce(),"color_flag",3);
set(gcf(), "background",130)
isoview

