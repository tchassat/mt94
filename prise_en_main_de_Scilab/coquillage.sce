//coquillage

phi=linspace(0,%pi,100)
teta=linspace(0,15*%pi,400)

m=-1/10;
k=3;
a=0.5;

b=a.*(abs(exp(2*m*%pi)-1)/exp(2*m*%pi)+1)*sqrt(1+k^2);
[PHI,TETA]=meshgrid(phi,teta);

X=(a+b.*cos(PHI)).*exp(m.*TETA).*cos(TETA);
Y=(a+b.*cos(PHI)).*exp(m.*TETA).*sin(TETA);
Z=(k*a+b.*sin(PHI)).*exp(m.*TETA);


figure(1)
clf
mesh(X,Y,Z)
set(gcf(),'color_map',jetcolormap(128));
surf(X,Y,Z)
set(gce(),'color_flag',3);
set(gcf(), "background",130)
