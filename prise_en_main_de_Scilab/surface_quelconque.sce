//surface quelquonque

x=linspace(-1,1,20);
y=linspace(-2,2,40);

[X,Y]=meshgrid(x,y);

clf
Z=cos(%pi*sqrt(X.^2+Y.^2));
mesh(X,Y,Z)

set(gcf(), "color_map", jetcolormap(128))

surf(X,Y,Z)
set(gce(),"color_flag",3)
set(gcf(), "background",130)
