// Approximation d'une derivée (méthode d'ordre un, deux, et du pas complexe)

// Comparaison des erreurs des différents schémas en fonction de h

function [y]=f(x)
    y=exp(x)./(cos(x).^3+sin(x).^3)
end
function d=fprime(x)
    d=exp(x)*((cos(x)^3 + 3*cos(x)^2*sin(x) - 3*cos(x)*sin(x)^2 + sin(x)^3))./(cos(x)^3 + sin(x)^3)^2;
endfunction


h=10.^(-20:0.1:-1) //vecteur de valeure de h
x0=%pi/4;

D1 = (f(x0+h)-f(x0))./h; //approximation d'ordre un de la dérivé
e1=abs((D1-fprime(x0))/fprime(x0)) //erreur 

D2 = (f(x0+h)-f(x0-h))./(2*h); //approximation d'ordre un de la dérivé
e2=abs((D2-fprime(x0))/fprime(x0)) //erreur 


Dc = imag(f(x0+%i*h))./h    //approximation à l'aide de la méthode du pas complexe
ec = abs((Dc-fprime(x0))/fprime(x0))
ec(ec==0)=10^-16

clf 
plot(h,e1,h,e2,h,ec)
legend("$D_1$","$D_2$", '$D_c$')
gca().log_flags="ll"
