// Regression de différents degrés

// l'execution de ce programme nécéssite la présence du fichier data1.sod dans le repertoire de travail

// il suffit de modifier p pour faire verier le degré du polyôme

load data1.sod
n=length(y); 
S = zeros(9,1);
p=3
    A=zeros(n,p+1); 
    for k=[0:p]
        A(:,k+1) = t.^k; 
    end 
    theta = A\y;
    subplot(3,3,3)
    plot(t,y,'o',t,A*theta,"r","thickness",2)
    title("p=3")



