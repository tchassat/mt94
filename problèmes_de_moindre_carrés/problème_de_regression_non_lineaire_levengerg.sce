//problème de régression non linéaire - méthode de levenberg-Marquardt

// l'execution de ce fichier nécessite la présente de 'dataGaussian.sod' dans le répertoire

clf

function r=resid(x)
    a = x(1);
    mu = x(2)
    sigma = x(3)
    r = a*exp(-(t-mu).^2/sigma^2) - y;
end


load dataGaussian.sod
plot(t,y,"o")

x= [1;1;1]

I= eye(3,3)
lambda=1
for k = 1:100
    r = resid(x)
    J = complexStepJac(resid,x)
    h = [J;sqrt(lambda)*I]\[r;0;0;0]
    x = x-h
    if norm(h) <1e-6
        break
    end
end

a = x(1);
mu = x(2)
sigma = x(3)
plot(u,a*exp(-(u-mu).^2/sigma^2),'r',"thickness",3)



