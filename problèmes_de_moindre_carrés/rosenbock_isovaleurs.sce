//rosenbock - iso valeurs

t = linspace(0,2*%pi,1000)

clf
isoview
gca().data_bounds=[-1 2 -1 2]
gca().auto_scale="off"

for alpha = 1:10:301
    x1 = 1-sqrt(alpha)*cos(t)
    x2=sqrt(alpha)/10*sin(t)+x1.^2;
    plot(x1,x2)
end
plot(1,1,'xr')
