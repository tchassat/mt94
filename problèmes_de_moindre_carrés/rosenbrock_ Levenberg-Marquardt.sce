// Fonction rosenbock - Méthode de Lebe,berg-Marquardt

function r = resid(x)
    r=[1-x(1);10*(x(2)-x(1)^2)]
end

function J=jac(x)
    J = [-1, 0
         -20*x(1), 10]
end
    
t = linspace(0,2*%pi,1000)

clf
isoview
gca().data_bounds=[-1 2 -1 2]
gca().auto_scale="off"

for alpha = 1:10:301
    x1 = 1-sqrt(alpha)*cos(t)
    x2=sqrt(alpha)/10*sin(t)+x1.^2;
    plot(x1,x2)
end
plot(1,1,'xr')

x=[-.5;1.5]
I= eye(2,2)
//lambda=10
for k = 1:100
    r = resid(x)
    J = jac(x)
    lambda = norm(r)
    h = [J;sqrt(lambda)*I]\[r;0;0]
    plot([x(1) x(1)-h(1)], [x(2) x(2)-h(2)],'-or')
    x = x-h
    if norm(h) <1e-6
        break
    end
end
