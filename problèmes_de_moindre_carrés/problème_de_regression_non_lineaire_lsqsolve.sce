//problème de régression non linéaire - utilisation de la fonction lsqrsolve

// l'execution de ce fichier nécessite la présente de 'dataGaussian.sod' dans le répertoire

clf

load dataGaussian.sod
plot(t,y,"o")

function r=resid(x,n)
    a = x(1);
    mu = x(2)
    sigma = x(3)
    r = a*exp(-(t-mu).^2/sigma^2) - y;
end

x0 = [1;1;1]
x = lsqrsolve(x0, resid, length(y))

a = x(1);
mu = x(2)
sigma = x(3)
plot(u,a*exp(-(u-mu).^2/sigma^2),'r',"thickness",3)
