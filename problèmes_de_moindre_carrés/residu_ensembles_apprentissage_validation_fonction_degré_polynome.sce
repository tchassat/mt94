// Résidu des ensembles d'apprentissage et de validation en fonctiondu degré du polynôme

// l'exectution de ce programme nécessite "data1.sod" dans le repertoire

load data1.sod
clf
plot(t,y,'o')
n=length(y);

T=25:n-25;
V=setdiff(1:n,T)
ST=zeros(9,1);
SV=zeros(9,1);

for p=0:8
    A=zeros(n,p+1);
    for k=0:p
        A(:,k+1)=t.^k
    end
    theta=A(T,:)\y(T)
    ST(p+1)=norm(A(T,:)*theta-y(T))^2
    SV(p+1)=norm(A(V,:)*theta-y(V))^2
end

clf 
plot(0:8,ST,'-o',0:8,SV,'-o')
gca().log_flags='nl'
legend('Apprentissage', 'Validation')
title('Résidu d apprentissage et de validation en fonction du degré du polynôme, en échelle semi logarithmique')
xlabel('degré du polynôme')
ylabel('résidu en échelle logarithmique')
