// Problème de régression non linéaire - méthode du log-trick

// l'execution de ce fichier nécessite la présente de 'dataGaussian.sod' dans le répertoire

clf
load dataGaussian.sod
plot(t,y,"o")

A = [ones(t), 2*t, -t.^2]
x = A\log(y)
sigma = 1/sqrt(x(3))
mu = x(2)/x(3)
a = exp(x(1)+mu^2/sigma^2)

u = linspace(mu-3*sigma, mu+3*sigma,1000)
plot(u,a*exp(-(u-mu).^2/sigma^2),'r')
