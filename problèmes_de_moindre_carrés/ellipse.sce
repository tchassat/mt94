// approximation d'une ellipse

// l'éxecution de ce programme nécessite la présence de 'data2.sod' dans le répertoire

load data2.sod
clf
plot(x,y,'o')
isoview 

A = [2*x.*y y.^2 -2*x -2*y ones(x)]

p=A\(-x.^2)

alpha = p(1)
bet = p(2)
M = [1 alpha; alpha bet];
c = M\p(3:4)
gam = sqrt(c'*M*c-p(5))

[P,D] = spec(M);
t = linspace(0,2*%pi,1000)
X=gam*P*[cos(t)/sqrt(D(1,1)); sin(t)/sqrt(D(2,2))]

plot(c(1)+X(1,:),c(2)+X(2,:),"r","thickness",3)
